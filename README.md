# Google Play Privacy Policy

I do not collect data. This applies to the following apps:

    https://play.google.com/store/apps/details?id=io.limeware.tidynote
    https://play.google.com/store/apps/details?id=io.limeware.noiseplayer
    https://play.google.com/store/apps/details?id=io.limeware.voicerecorder
